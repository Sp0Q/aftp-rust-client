use std::net::{TcpStream};
use std::io::{Write};
use std::io::{prelude::*, BufReader};
use std::str;

const CONTENTLENGTH: &'static str = "Content-Length";

pub fn send_list(mut stream: &TcpStream) -> Option<Vec<String>> {
    let cmd = String::from("LIST / AFTP/1.0\r\n\r\n");
    stream.write(cmd.as_bytes()).expect("Could not send list request");
    println!(" => list request sent");

    let mut reader = BufReader::new(stream);

    let mut content_length: usize = 0;

    // Parse headers
    // TODO own function
    for line in reader.by_ref().lines() {
        let content = line.expect("Could not read LIST line");

        if content == "200 OK" {
            continue;
        } else if content == "404 Not found" {
            println!("<= 404 Not found");
            break;
            // TODO handle other errors => if not 200 OK => don't go further
        } else if content.is_empty() {
            break;
        }

        // BUG split on whitespace, could go wrong if header value has spaces
        let mut values = content.split_whitespace();

        let header = values.next().unwrap().trim_end_matches(":");

        match header {
            CONTENTLENGTH => {
                let value = values.next().unwrap();
                content_length = value.parse::<usize>().unwrap();
                println!("content-length: {}", content_length);
            },
            _ => {
                println!("Unknown header: {}", header);
            }
        }
    }

    // Parse content
    // TODO own function
    if content_length > 0 {
        let mut dir: Vec<String> = Vec::new();

        let mut buf = vec![0; content_length];
        reader.by_ref().read_exact(&mut buf).expect("Could not read stream into buf");

        let content = match str::from_utf8(&mut buf) {
            Ok(content) => content,
            Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
        };

        for line in content.lines() {
            &dir.push(line.to_owned());
        }

        return Some(dir);
    } else {
        return None;
   }
}