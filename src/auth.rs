use std::str::FromStr;
use std::net::{TcpStream};
use std::io::{Read, Write};
use rand::Rng;

pub fn send_auth(mut stream: &TcpStream) {
    // Send auth request
    stream.write(b"AUTH AFTP/1.0\r\n\r\n").expect("Could not send auth request");
    println!(" => auth request sent");

    let response = &mut [0; 128];
    stream.read(response).expect("Could not read AUTH response");
    let response_string = String::from_utf8_lossy(response);

    // parse pubkey
    let some_pubkey = PubKey::new(&response_string);

    match some_pubkey {
        Some(pubkey) => {
            handle_auth(pubkey, &stream);
        },
        None => {
                println!("{} is not a valid PubKey!", &response_string);
        },
    }
}

fn handle_auth(pubkey: PubKey, mut stream: &TcpStream) -> u128 {
    let mut rng = rand::thread_rng();
    let n: u32 = rng.gen_range(2, 25);
    let response = &mut [0; 128];

    println!(
        r"The PubKey is: p: {} g: {} t: {}",
        pubkey.p, pubkey.g, pubkey.t
    );

    let t :u128 = pubkey.g.pow(n) % pubkey.p as u128;
    println!("{}^{} % {} = {}", pubkey.g, n, pubkey.p, t);

    let pub_key_msg = format!("PUB {} AFTP/1.0\r\n\r\n", t);
    stream.write(pub_key_msg.as_bytes()).unwrap();

    stream.read(response).expect("Could not read PUB response");
    let response_string = String::from_utf8_lossy(response);
    println!("response: {}", &response_string);

    let shared_key = (pubkey.t as u128).pow(n) % pubkey.p as u128;
    println!("{}^{} % {} = {}", pubkey.t, n, pubkey.p, shared_key);
    println!("shared key: {}", shared_key);

    return shared_key;
}

#[derive(Debug, PartialEq)]
pub struct PubKey {
    pub p: u32,
    pub g: u128,
    pub t: u32,
}

impl PubKey {
    pub fn new(input: &str) -> Option<PubKey> {
        match PubKey::from_str(&input) {
            Ok(pubkey) => Some(pubkey),
            Err(_) => None
        }
    }
}

impl FromStr for PubKey {
    type Err = std::num::ParseIntError;

    // Parses a pubkey of the form 'p=<u8>/r/ng=<u8>' into an
    // instance of 'PubKey'
    fn from_str(input: &str) -> Result<Self, Self::Err> {
    
        let p: u32 = get_val_from_keyval(input, "p").unwrap().parse::<u32>().unwrap();
        let g: u128 = get_val_from_keyval(input, "g").unwrap().parse::<u128>().unwrap();
        let t: u32 = get_val_from_keyval(input, "t").unwrap().parse::<u32>().unwrap();

        Ok(PubKey { p, g, t })
    }
}

fn get_val_from_keyval(keyval: &str, key: &str) -> Option<String> {
    let lines = keyval.lines();

    for line in lines {
        let mut keyval = line.split("=");
        let cur_key = keyval.next().unwrap();
        if cur_key == key {
            let value = keyval.next().unwrap().trim_matches(char::from(0));
            return Some(value.to_string());
        }
    }
    
    return None;
}
