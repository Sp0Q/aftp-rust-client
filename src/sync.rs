use std::fs;
use std::fs::File;
use std::path::{PathBuf, Path};
use std::net::TcpStream;
use std::io::{prelude::*, BufReader, Read, Write};
use std::convert::TryInto;
use std::time::SystemTime;

const CONTENTLENGTH: &'static str = "Content-Length";

pub fn handle_list(root_dir: &Path, list: &Vec<String>, stream: &TcpStream) {
    // parse to folder which should be synced with server
    println!("directory to sync: {:?}", root_dir);

    // get tree of local directory
    // check list against tree
    // if not exists -> delete or download

    // handle dir
    for entry in list {
        // parse line
        let etag = &entry[entry.len()-32..entry.len()];
        let modified: u64 = entry[entry.len()-43..entry.len()-33].parse().expect("Could not parse modified to u64");
        let filepath = &entry[..entry.len()-44];

        println!("etag: '{}'", etag);
        println!("modified: '{}'", modified);
        println!("filepath: '{}'", filepath);

        // prepend root_dir to entry
        let mut full_dir = PathBuf::new();
        full_dir.push(&root_dir);
        full_dir.push(&filepath);

        // prevent path traversal attacks
        if !full_dir.starts_with(&root_dir) {
            println!("NOPE");
            panic!("Path traversal attack!");
        }

        // check if file exists => open directly to prevent race condition
        let file_read = File::open(&full_dir);
        // check if file_read is ok, same as .expect("could not open file")
        match file_read.ok() {
            Some(file_unwrapped) => {
                println!("file exists!");
                // TODO check which is newer and update the other one if different

                handle_file(&stream, &file_unwrapped, &etag, modified, &filepath, &full_dir);
            },
            None => {
                println!("file doesn't exist...");

                // Download file
                create_folders(&full_dir);

                let mut file_write = File::create(&full_dir).expect("could not create file");

                let file_content: Option<Vec<u8>> = download_file(&stream, &filepath);

                // pattern match
                if let Some(c) = &file_content {
                    file_write.write_all(&*c).expect("could not write to file");
                }
            }
        }
    }

    // TODO check own files for newly created stuff and upload it
    // get list of own files and compare with processed list, unprocessed local files are new or are deleted at the server
    // ask user to confirm new/delete file
}

fn create_folders(full_dir: &PathBuf) {
    // Download file
    println!("creating file: {:?}", &full_dir);

    // todo check if folders exist before creating
    let parent_folder = &full_dir.parent();
    if let Some(f) = &parent_folder {
        if !&f.exists() {
            fs::create_dir_all(&f).expect("could not create directories");
        }
    }
}

// returns the file bytes without the headers
fn download_file(mut stream: &TcpStream, entry: &str) -> Option<Vec<u8>> {
    println!("Downloading {}", &entry);

    let mut cmd = String::from("GET /");
    cmd.push_str(&entry);
    cmd.push_str(&String::from(" AFTP/1.0\r\n\r\n"));
    stream.write(cmd.as_bytes()).expect("Could not send GET request");
    println!(" => get request sent");

    let mut reader = BufReader::new(stream);

    let mut content_length: usize = 0;

    // Parse headers
    // TODO own function
    for result in reader.by_ref().lines() {

        let content = result.expect("Could not read GET line");

        if content == "200 OK" {
            continue;
        } else if content == "404 Not found" {
            println!("<= 404 Not found");
            break;
            // TODO handle other errors => if not 200 OK => don't go further
        } else if content.is_empty() {
            break;
        }

        let mut values = content.split_whitespace();

        let header = values.next().unwrap().trim_end_matches(":");

        match header {
            CONTENTLENGTH => {
                let value = values.next().unwrap();
                println!("{:?}", value);
                content_length = value.parse::<usize>().unwrap();
                println!("content-length: {}", content_length);
            },
            _ => {
                println!("Unknown header: {}", header);
            }
        }
    }

    // Parse content
    // TODO own function
    if content_length > 0 {
        let mut buf = vec![0; content_length];
        reader.by_ref().read_exact(&mut buf).expect("Could not read stream into buf");

        return Some(buf);
    } else {
        return None;
   }
}

fn handle_file(stream: &TcpStream, file: &File, etag: &str, modified: u64, filepath: &str, full_dir: &PathBuf) {
    let metadata = file.metadata().unwrap();
    let mut buf_reader = BufReader::new(file);

    let mut buf = vec![0; metadata.len().try_into().unwrap()];
    buf_reader.read(&mut buf).expect("Could not read file content");

    // if etag differs, else do nothing
    let digest = md5::compute(&mut buf);
    let local_etag = format!("{:x}", digest).as_str().to_owned();

    if etag.eq_ignore_ascii_case(&local_etag.as_str()) {
        println!("files are the same");
    } else {
        println!("files are different");

        let local_modified = metadata.modified().expect("could not get last modified date of file")
            .duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();

        // if server modified => client modified: download server version
        if local_modified > modified {
            println!("server version is older");
            upload_file(&stream, &filepath, &full_dir, &etag);
        } // if client modified > server modified: put client version
        else {
            println!("server version is newer");

            create_folders(&full_dir);

            let mut file_write = File::create(&full_dir).expect("could not create file");
            let file_content: Option<Vec<u8>> = download_file(&stream, &filepath);

            // pattern match
            if let Some(c) = &file_content {
                file_write.write_all(&*c).expect("could not write to file");
            }
        }
    }
}

fn upload_file(mut stream: &TcpStream, entry: &str, full_dir: &PathBuf, etag: &str) {
    let file_read = File::open(&full_dir);
    // check if file_read is ok, same as .expect("could not open file")
    match file_read.ok() {
        Some(file_unwrapped) => {
            let mut cmd = String::from("PUT /");
            cmd.push_str(&entry);
            cmd.push_str(&String::from(" AFTP/1.0\r\n"));
            
            let metadata = &file_unwrapped.metadata().unwrap();
            let mut buf_reader = BufReader::new(&file_unwrapped);
            let mut buf = vec![0; metadata.len().try_into().unwrap()];
            buf_reader.read(&mut buf).expect("Could not read file");

            // ETag
            cmd.push_str(&String::from("ETag: "));
            cmd.push_str(&etag);
            cmd.push_str("\r\n");

            // Content-Length
            if buf.len() > 0 {
                cmd.push_str(&String::from("Content-Length: "));
                cmd.push_str(buf.len().to_string().as_str());
                cmd.push_str("\r\n");
            }

            // Empty line
            cmd.push_str("\r\n");

            stream.write(&buf).expect("Could not send PUT request");
            println!(" => put request sent");

            let response = &mut [0; 128];
            stream.read(response).expect("Could not read PUT response");
            let response_string = String::from_utf8_lossy(response);
            println!("response: {}", &response_string);
        },
        None => {
            println!("Could not open file");
        }
    }
}